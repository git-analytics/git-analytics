import su from './spawn-utils'

function splitToCommitMessages(gitOutputString) {
  const regex = /(^|\n)commit [0-9a-f]{40}/gi
  let result = gitOutputString.split(regex)
  result = result.map(r => r.trim()).filter(r => r)
  return result
}

function parseFilesChanged(filesChangedStr) {
  const filesChangedMatch = /([0-9]+) files? changed/.exec(filesChangedStr)
  const insertionsMatch = /([0-9]+) insertions?/.exec(filesChangedStr)
  const deletionsMatch = /([0-9]+) deletions?/.exec(filesChangedStr)

  const filesChanged = (filesChangedMatch && filesChangedMatch[1] && Number(filesChangedMatch[1])) || 0
  const insertions = (insertionsMatch && insertionsMatch[1] && Number(insertionsMatch[1])) || 0
  const deletions = (deletionsMatch && deletionsMatch[1] && Number(deletionsMatch[1])) || 0
  return {
    filesChanged,
    insertions,
    deletions
  }
}

function parseCommitMessage(commitMessage) {
  const regex = /Author: (.*?) <.*?>\nDate:[\s]*(.*?)\n(.*)/is
  const rgMatch = regex.exec(commitMessage)
  if (!rgMatch) {
    console.error('Cannot parse:', commitMessage)
    return null
  }
  const lastLines = rgMatch[3]
    .split('\n')
    .map(l => l.trim())
    .filter(l => l)
  const filesChanged = lastLines[lastLines.length - 1]
  const message = lastLines.slice(0, lastLines.length - 1).join('\n')

  const ticketsFixed = []
  const ticketsFixedRegex = /(fix|fixed|resolve|resolved) ([A-Z0-9]+-[0-9]+)/gis
  let tfMatch = ticketsFixedRegex.exec(message)
  while (tfMatch) {
    ticketsFixed.push(tfMatch[2])
    tfMatch = ticketsFixedRegex.exec(message)
  }

  return {
    author: rgMatch[1],
    date: new Date(rgMatch[2]),
    message,
    ...parseFilesChanged(filesChanged),
    ticketsFixed
  }
}

function getDateToCompare(dateString) {
  if (!dateString) return null
  const d = new Date()

  if (dateString == 'day') return new Date(d.getTime() - 1000 * 60 * 60 * 24)
  if (dateString == 'week') return new Date(d.getTime() - 1000 * 60 * 60 * 24 * 7)
  if (dateString == '2weeks') return new Date(d.getTime() - 1000 * 60 * 60 * 24 * 14)
  if (dateString == 'month') return new Date(d.getTime() - 1000 * 60 * 60 * 24 * 30)
  if (dateString == 'year') return new Date(d.getTime() - 1000 * 60 * 60 * 24 * 365)
  const daysNumber = Number(dateString)
  if (daysNumber) return new Date(d.getTime() - 1000 * 60 * 60 * 24 * daysNumber)

  return new Date(dateString)
}

async function run() {
  const result = await su.spawn('git log --diff-filter=ACMD --use-mailmap --no-merges --shortstat', [], {
    cwd: process.argv[2]
  })

  // const regex = /commit [0-9a-f]{40}\nAuthor: (.*?) <.*?>\nDate:[\s]*(.*?)\n([.\n\r\s\S]*?)/gi
  // const commitStartRegex = /commit [0-9a-f]{40}\nAuthor: (.*?) <.*?>\nDate:[\s]*(.*?)\n([.\n\r\s\S]*?)/gi
  const commitMessages = splitToCommitMessages(result)
  let commitMetas = commitMessages.map(c => parseCommitMessage(c))

  const dateToCompare = getDateToCompare(process.argv[3])
  if (dateToCompare) {
    commitMetas = commitMetas.filter(cm => cm.date > dateToCompare)
  }

  const commitMetasByAuthor = {}
  const ticketsFixedByAuthor = {}

  for (const cm of commitMetas) {
    if (!commitMetasByAuthor[cm.author]) {
      commitMetasByAuthor[cm.author] = {
        // commitMetas: [],
        totalTicketsFixed: 0,
        totalInsertions: 0,
        totalDeletions: 0,
        totalChanges: 0,
        totalFilesChanged: 0,
        totalCommits: 0
      }
      ticketsFixedByAuthor[cm.author] = {}
    }

    const mba = commitMetasByAuthor[cm.author]
    // mba.commitMetas.push(cm)
    mba.totalInsertions += cm.insertions
    mba.totalDeletions += cm.deletions
    mba.totalChanges += cm.deletions + cm.insertions
    mba.totalFilesChanged += cm.filesChanged
    mba.totalCommits++

    for (const tf of cm.ticketsFixed) {
      if (!ticketsFixedByAuthor[cm.author][tf]) {
        ticketsFixedByAuthor[cm.author][tf] = true
        mba.totalTicketsFixed++
      }
    }
  }

  console.log(commitMetasByAuthor)
}

run()