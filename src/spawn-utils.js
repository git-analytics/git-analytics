import childProcess from 'cross-spawn'

function spawn(cmd, args = [], options = undefined) {
  return new Promise((resolve, reject) => {
    var child = childProcess.spawn(cmd, args, options)
    var result = ''
    child.stdout.on('data', function(data) {
      result += data.toString()
      // console.log(data.toString())
    })
    child.stderr.on('data', function(data) {
      console.error(data.toString())
    })
    child.on('exit', function(code) {
      if (code > 0) reject(code)
      else resolve(result)
    })
  })
}

export default {
  spawn
}
